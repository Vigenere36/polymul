CC = g++
CFLAGS = -g -Wall -std=c++0x
LFLAGS = 
TARGET = mul

SRC = $(wildcard src/*.cpp)
OBJ = $(SRC:.cpp=.o)

all: $(TARGET)

$(TARGET): $(OBJ) main.cpp
	$(CC) $(OBJ) main.cpp $(LFLAGS) -o $(TARGET)

.cpp.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	rm -f $(OBJ) $(TARGET)