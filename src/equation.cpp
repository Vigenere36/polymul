#include "equation.h"

//---------------OPERATIONS-----------------------------

void Equation::add(const int index, const float value) {
	//Either adds a value to an existing coefficient, or sets the value
	if (index <= getDegree()) {
		set(index, value + get(index));
	} else {
		set(index, value);
	}
}

void Equation::negate(const int n) {
	//Negate the (n+1)th nonzero term from the equation
	int term = 0;
	for (int i = 0; i < coefficients.size(); i++) {
		if (get(i) != 0) {
			if (n == term) {
				set(i, -1 * get(i));
				return;
			}
			term++;
		}
	}
	std::cout << "Negation failed\n";
}

Equation Equation::add(const Equation& other) const {
	//Return the result of adding two equations
	Equation sum;
	for (int i = 0; i <= std::max(getDegree(), other.getDegree()); i++) {
		sum.set(i, get(i) + other.get(i));
	}
	return sum;
}

Equation Equation::sub(const Equation& other) const {
	//Return the result of subtracting two equations
	Equation negated = other.mul(-1);
	return add(negated);
}

Equation Equation::mul(const float scale) const {
	//Return the result of multiplying an equation by a scale
	Equation product;
	for (int i = 0; i <= getDegree(); i++) {
		product.set(i, get(i) * scale);
	}
	return product;
}

RI root(const int exp, const int roots) {
	RI toReturn;
	float angle = ((float)exp*360.0f)/(float)roots * (M_PI/180.0f);
	toReturn.setReal(cos(angle));
	toReturn.setImag(sin(angle));
	return toReturn;
}

void align(RI& complex) {
	float r = (float)((int)(complex.getReal() * 1000.0f));
	float i = (float)((int)(complex.getImag() * 1000.0f));
	complex.setReal(r/1000.0f);
	complex.setImag(i/1000.0f);
	return;
}

/*
	FFT Example:
	P = 1 + 3x + 5x^2 + 7x^3 + 8x^4 + 6x^5 + 3x^6 + 2x^7
	Evaluate at 8 roots of unity

	---------
	Step 1
	Evaluate at 4 roots of unity
	
	Pe = 1 + 5x + 8x^2 + 3x^3
		Pe(1) = 9 + (1)8 = 17			w^0 = 1 			(power/2) mod 2?
		Pe(i) = -7 + (i)2 = -7 + 2i		w^(1*2) = -1
		Pe(-1) = 9 + (-1)8 = 17 		w^(2*2) = 1
		Pe(-i) = -7 + (-i)2 = 7 - 2i	w^(3*2) = -1
	Po = 3 + 7x + 6x^2 + 2x^3
	P(x) = Pe(x^2) + xPo(x^2)
	P(-x) = Pe(x^2) - xPo(x^2)

	----------
	Step 2
	Evaluate at 2 roots of unity

	Pee = 1 + 8x -> Pee(1) = 9, Pee(-1) = -7
	Peo = 5 + 3x -> Peo(1) = 8, Peo(-1) = 2
	Pe(x) = Pee(x^2) + xPeo(x^2)
	Pe(-x) = Pee(x^2) - xPeo(x^2)

	Poe = 3 + 6x -> Poe(1) = 9, Poe(-1) = -3
	Poo = 7 + 2x -> Poo(1) = 8, Poo(-1) = 5
	Po(x) = Poe(x^2) + xPoo(x^2)
	Po(-x) = Poe(x^2) - xPoo(x^2)
*/
std::vector<RI> FFT(std::vector<RI> vals, int power) {
	int roots = vals.size();
	if (roots == 2) {
		std::vector<RI> toReturn;
		RI even = vals[0].add(vals[1]);
		RI odd = vals[0].sub(vals[1]);
		toReturn.push_back(even);
		toReturn.push_back(odd);
		return toReturn;
	} else {
		//Get the values from the recursive call
		std::vector<RI> evenVals;
		std::vector<RI> oddVals;
		for (int i = 0; i < vals.size(); i++) {
			if (i % 2 == 0) {
				evenVals.push_back(vals[i]);
			} else {
				oddVals.push_back(vals[i]);
			}
		}

		std::vector<RI> even = FFT(evenVals, power);
		std::vector<RI> odd = FFT(oddVals, power);
		std::vector<RI> toReturn;

		//Return values after evaluating
		for (int i = 0; i < vals.size(); i++) {
			int index = i % (vals.size()/2);
			RI rooti= root(power * i, vals.size());
			RI rhs = odd[index].mul(rooti);
			RI result = even[index].add(rhs);
			align(result); //trying to remove floating point error
			toReturn.push_back(even[index].add(rhs));
		}
		return toReturn;
	}
}

Equation Equation::mul(const Equation& other) {
	//Perform FFT on both, multiply, then interpolate

	//Pad each vector, then make a power of two
	std::vector<float> poly1_f(coefficients);
	std::vector<float> poly2_f(other.coefficients);
	poly1_f.resize(poly1_f.size() + poly2_f.size() - 1, 0);
	poly2_f.resize(poly1_f.size() + poly2_f.size() - 1, 0);
	float sizelog2 = log2(std::max(poly1_f.size(), poly2_f.size()));
	int power = (int)pow(2, ceil(sizelog2));
	poly1_f.resize(power, 0);
	poly2_f.resize(power, 0);

	//Convert to RI
	std::vector<RI> poly1;
	std::vector<RI> poly2;
	for (int i = 0; i < power; i++) {
		poly1.push_back(RI(poly1_f[i], 0));
		poly2.push_back(RI(poly2_f[i], 0));
	}

	//Perform evaluation
	std::vector<RI> values1 = FFT(poly1, 1);
	std::vector<RI> values2 = FFT(poly2, 1);

	//Do element wise multiplication
	std::vector<RI> values3;
	for (int i = 0; i < values1.size(); i++) {
		values3.push_back(values1[i].mul(values2[i]));
	}

	//Perform interpolation
	std::vector<RI> poly3 = FFT(values3, -1);
	//Scale by (1/n), and store in equation
	float scale = 1/(float)poly3.size();
	Equation result;

	for (int i = 0; i < poly3.size(); i++) {
		poly3[i] = poly3[i].mul(scale);
		result.set(i, poly3[i].getReal());
	}

	return result;
}

//----------------DEBUGGING------------------------------
void Equation::printCoeff() const {
	for (int i = 0; i < coefficients.size(); i++) {
		std::cout << coefficients[i] << " ";
	}
	std::cout << std::endl;
}

void Equation::print() const {
	//Prints the equation
	if (coefficients.size() == 0) {
		std::cout << "Equation empty";
	}

	bool first = true;
	for (int i = 0; i <= getDegree(); i++) {
		float c = get(i);
		if (c != 0 && fabs(c) > EPSILON) {
			if (!first && c < 0) {
				std::cout << " - ";
			} else if (!first && c > 0) {
				std::cout << " + ";
			} else if (first && c < 0) {
				std::cout << "-";
			}

			if (fabs(c) != 1 || i == 0) {
				if (fabs(ceil(c) - c) < EPSILON) {
					std::cout << fabs(ceil(c));				
				} else if (fabs((int)c - c) < EPSILON) {
					std::cout << fabs((int)c);			
				} else {
					std::cout << fabs(c);
				}
			}

			if (i == 1) {
				std::cout << "x";
			} else if (i != 0) {
				std::cout << "x^" << i;
			}

			first = false;
		}
	}
	std::cout << std::endl;
}