#include <iostream>
#include <forward_list>
#include <cstdlib>
#include <stdio.h>
#include "src/equation.h"
#include "src/debug.h"

#define MAX_LINE_LEN 10000

std::forward_list<Equation> equations;

Term parseToken(char* tok) {
	//Parse a single term of the equation
	struct Term t = {0,0};
	bool power = false;
	std::string coeff = "";
	std::string expon = "";

	//Store coefficient and exponent in separate strings
	for (int i = 0; i < strlen(tok); i++) {
		if (isalpha(tok[i])) {
			power = true;
		} else if (isdigit(tok[i])) {
			if (!power) {
				coeff += tok[i];
			} else {
				expon += tok[i];
			}
		} else if (tok[i] == '.') {
			if (!power) {
				coeff += '.';
			} else {
				perror("Exponent must be integral (using the Discrete Fourier Transform)");
				exit(1);
			}
		}
	}

	//Get coeffient and exponent out of those strings
	if (coeff.empty()) {
		t.coeff = 1;
	} else {
		t.coeff = std::stof(coeff);
	}

	if (power == true && expon.empty()) {
		t.power = 1;
	} else if (expon.empty()) {
		t.power = 0;
	} else {
		t.power = std::stoi(expon);
	}

	return t;
}

void parse(FILE* input) {
	//Parses input and closes stream
	if (!input) {
		std::cout << "Invalid file name\n";
		fclose(input);
		exit(1);
	}

	char line[MAX_LINE_LEN];
	while (fgets(line, MAX_LINE_LEN, input)) {
		Equation eq;
		char* tknsrc = (char*)calloc(strlen(line), sizeof(char));
		strcpy(tknsrc, line);
		char* tkn;

		//Place terms in equation
		while ((tkn = strtok(tknsrc, "+-")) != NULL) {
			tknsrc = NULL; //For further calls to strtok
			struct Term t = parseToken(tkn);
			eq.add(t.power, t.coeff);
		}
		delete tknsrc;
		tknsrc = NULL;

		//Negate some terms depending on '-' signs
		int term = 0;
		for (int i = 0; i < strlen(line); i++) {
			if (line[i] == '+' || line[i] == '-') {
				if (line[i] == '-') {
					char* tmp = (char*)calloc(strlen(line) - i - 1, sizeof(char));
					strcpy(tmp, line + i + 1);
					struct Term t = parseToken(strtok(tmp, "+-"));
					eq.add(t.power, -2 * t.coeff); //-2 because we're both negating addition and doing subtraction
					delete tmp;
					tmp = NULL;
				}
				term++;
			}
		}

		equations.push_front(eq);
		//eq.print();
	}

	fclose(input);
}

void test() {
	//Test addition. Not using it anyway though...
	Equation sum;
	std::forward_list<Equation>::iterator iter;
	for (iter = equations.begin(); iter!= equations.end(); ++iter) {
		sum = sum.add(*iter);
	}
	sum.print();
}

void reduce() {
	if (equations.empty()) return;
	Equation product = equations.front();
	equations.pop_front();
	if (equations.empty()) {
		equations.push_front(product);
		return;
	}
	product = product.mul(equations.front());
	equations.pop_front();

	reduce();

	equations.push_front(product);
}

void write(Equation& e) {
	FILE* output = fopen("output.txt", "w");
	char* toWrite = (char*) calloc(MAX_LINE_LEN, sizeof(char));
	for (int i = 0; i <= e.getDegree(); i++) {
		if (i == 0) {
			sprintf(toWrite, "%f", e.get(i)); //Plz don't buffer overflow me
			fputs(toWrite, output);
		} else if (fabs(e.get(i)) > EPSILON) {
			if (e.get(i) < 0) {
				sprintf(toWrite, " - ");
				fputs(toWrite, output);
			} else if (e.get(i) > 0){
				sprintf(toWrite, " + ");
				fputs(toWrite, output);				
			}

			//This actually isn't helping much
			if (fabs(ceil(e.get(i)) - e.get(i)) < EPSILON) {
				sprintf(toWrite, "%f" , fabs(ceil(e.get(i))));
				fputs(toWrite, output);				
			} else if (fabs((int)e.get(i) - e.get(i)) < EPSILON) {
				sprintf(toWrite, "%f" , fabs((int)e.get(i)));
				fputs(toWrite, output);				
			} else {
				sprintf(toWrite, "%f" , fabs(e.get(i)));
				fputs(toWrite, output);
			}

			if (i == 1) {
				sprintf(toWrite, "x");
				fputs(toWrite, output);
			} else if (i != 0) {
				sprintf(toWrite, "x^%d", i);
				fputs(toWrite, output);
			}
		}
	}
}

int main(int argc, char* argv[]) {
	signal(SIGSEGV, handler);
	if (argc != 2) {
		std::cout << "Usage: mul [filename]\n";
		exit(1);
	}

	FILE* input = fopen(argv[1], "r");
	parse(input);
	fclose(input);
	
	Equation product;
	while (true) {
		reduce();
		product = equations.front();
		equations.pop_front();
		if (equations.empty()) break;
		equations.push_front(product);
	}

	write(product);
	product.print();
}