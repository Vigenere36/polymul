# PolyMul #

A small side project that utilizes the Fast Fourier Transform to perform polynomial multiplication.

Inspiration from CS170.

A little buggy, especially with more than 6 equations. Also, a ton of annoying floating point error.

### Usage ###

* Open up terminal
* cd to directory
* Type "make"
* Type "./mul <filename>"

### File format ###

* Refer to sample.txt for example
* Type polynomials with a line break in between them
* Supports single variable equations
* Exponents must be integer-valued

### Thank you for checking this out! ###