#ifndef RI_H
#define RI_H

#include <iostream>
#include <vector>
#include <cmath>

class RI {
	float real;
	float imag;
public:
	//---------------CONSTRUCTORS--------------
	RI() {
		real = 0;
		imag = 0;
	}

	RI(float _real, float _imag) {
		real = _real;
		imag = _imag;
	}

	virtual ~RI() {
	}

	//---------------ACCESSORS----------------
	float getReal() const {
		return real;
	}

	float getImag() const {
		return imag;
	}

	void setReal(const float r) {
		real = r;
	}

	void setImag(const float i) {
		imag = i;
	}

	//--------------OPERATIONS-----------------
	RI add(const RI& other) const {
		RI sum;
		sum.real = real + other.real;
		sum.imag = imag + other.imag;
		return sum;
	}

	RI sub(const RI& other) const {
		RI sum;
		sum.real = real - other.real;
		sum.imag = imag - other.imag;
		return sum;
	}

	RI mul(RI& other) {
		RI product;
		product.real += real * other.real;
		product.real -= imag * other.imag;
		product.imag += real * other.imag;
		product.imag += imag * other.real;
		return product;
	}

	RI mul(float scale) {
		RI product;
		product.real = real*scale;
		product.imag = imag*scale;
		return product;
	}

	//-------------DEBUGGING----------------
	void print() const {
		if (imag < 0) {
			std::cout << "(" << real << " - " << abs(imag) << "i)\n";
		} else {
			std::cout << "(" << real << " + " << imag << "i)\n";
		}
	}
};

#endif