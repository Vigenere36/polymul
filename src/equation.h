#ifndef EQUATION_H
#define EQUATION_H

#include <vector>
#include <cmath>
#include "RI.h"
#define EPSILON 0.01

struct Term {
	int power;
	float coeff;
};

class Equation {

std::vector<float> coefficients;

public:
	//--------------CONSTRUCTORS-------------------------
	Equation() {
	}

	Equation(const Equation& other) {
		for (int i = 0; i < other.coefficients.size(); i++) {
			coefficients.push_back(other.coefficients[i]);
		}
	}

	~Equation() {
	}

	Equation& operator=(const Equation& other) {
		coefficients = std::vector<float>();
		for (int i = 0; i < other.coefficients.size(); i++) {
			coefficients.push_back(other.coefficients[i]);
		}
		return *this;
	}

	//--------------GETTERS AND SETTERS-------------------
	virtual float get(const int index) const {
		//Gets a value for a given degree
		if (index < 0 || index + 1 > coefficients.size()) {
			//std::cout << "get: Invalid index: " << index << std::endl;
			return 0;
		}
		return coefficients[index];
	}

	virtual int getDegree() const {
		if (coefficients.empty()) return 0;
		//Gets the degree of the equation
		int degree;
		for (int i = 0; i < coefficients.size(); i++) {
			if (coefficients[i] != 0) degree = i;
		}
		return degree;
	}

	virtual void set(const int index, const float value) {
		//Sets a value for a given degree
		if (index < 0) {
			std::cout << "set: Invalid index: " << index << std::endl;
		} else if (index >= getDegree()) {
			coefficients.resize(index + 1, 0);
		}
		coefficients[index] = value;
	}

	//-------------OPERATIONS--------------------------

	virtual void add(const int index, const float value);
	virtual void negate(const int n);
	virtual Equation add(const Equation& other) const;
	virtual Equation sub(const Equation& other) const;
	virtual Equation mul(const float scale) const;
	virtual Equation mul(const Equation& other);

	//-------------DEBUGGING--------------------------

	virtual void printCoeff() const;
	virtual void print() const;
};

#endif